# Overview

This is an Android application for receiving video and telemetry data from a remote vehicle 
with a Cleanflight-based flight controller and an embedded Linux system such as a Raspberry
Pi.  See INSTALLATION.md in the droidhud/telewii project for more detailed instructions for
the vehicle-side software.

# Building

* Clone droidhud/gst-plugins-bad-droidhud and follow the instructions in README.md to build
* Modify `app/src/main/jni/build-sdk.sh` and set `GSTREAMER_ROOT_ANDROID` to point to 
  where you unpacked/installed Gstreamer.  Also modify `NDK_BUILD` to set the path to your NDK
  installation.
* Run `./build-ndk.sh`
* Open the project in Android Studio 2.0+ and build.

# Usage
* Power up your vehicle that has the telewii software installed and configured
* (Recommended) Turn on airplane-mode on your phone
* Enable Wi-Fi on your phone
* Connect to the vehicle's access point using your phone's WiFi
* Start the DroidHUD app
* Configure the settings as desired.  Note that you will need to calibrate the camera elevation and FOV
  to get proper pitch ladder alignment.
* Tap Start HUD!
* Insert phone into your Cardboard Viewer

# WARNING
* Please follow all local laws when operating your vehicle with this software.  
* Per the LICENSE, there is no warranty offered 
* It is up to you to ensure safe operation at all times  
* Test your setup with someone else using the viewer before flying FPV
* Use a spotter when flying FPV
