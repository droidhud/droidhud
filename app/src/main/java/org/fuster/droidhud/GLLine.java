package org.fuster.droidhud;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.content.res.Resources;
import android.graphics.Color;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

public class GLLine {
    private static final String TAG = "GLLine";

    private static final int COORDS_PER_VERTEX = 3;
    private static final float COLOR_CONV = (1.0f/255.0f);

    private int mMVPParam;
    private int mPositionParam;
    private int mColorParam;
    private int mColorAltParam;
    private int mColorLenParam;

    private int mProgram;


    public GLLine(Resources res, int maxLines) {


        int mVertexShader = GLUtils.loadGLShader(TAG, res, GLES20.GL_VERTEX_SHADER, R.raw.line_vertex);
        int mFragmentShader = GLUtils.loadGLShader(TAG, res, GLES20.GL_FRAGMENT_SHADER, R.raw.line_fragment);

        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, mVertexShader);
        GLES20.glAttachShader(mProgram, mFragmentShader);
        GLES20.glLinkProgram(mProgram);
        GLES20.glUseProgram(mProgram);

        GLUtils.checkGLError(TAG, "program");

        mPositionParam = GLES20.glGetAttribLocation(mProgram, "a_Position");
        mMVPParam = GLES20.glGetUniformLocation(mProgram, "u_MVP");
        mColorParam = GLES20.glGetUniformLocation(mProgram, "u_Color");
        mColorAltParam = GLES20.glGetUniformLocation(mProgram, "u_ColorAlt");
        mColorLenParam = GLES20.glGetUniformLocation(mProgram, "u_ColorLen");

        GLUtils.checkGLError(TAG, "program params");
    }

    public static FloatBuffer initFloatBuffer(int numLines) {
        ByteBuffer bbVertices = ByteBuffer.allocateDirect(numLines * 2 * (COORDS_PER_VERTEX) * 4);
        bbVertices.order(ByteOrder.nativeOrder());
        return bbVertices.asFloatBuffer();
    }

    public void draw(float[] mvpMatrix, FloatBuffer vertices, float width) {
        draw(mvpMatrix, vertices, width, Color.WHITE);
    }

    public void draw(float[] mvpMatrix, FloatBuffer vertices, float width, int color) {
        draw(mvpMatrix, vertices, width, color, 1.0f, color);
    }

    public void draw(float[] mvpMatrix, FloatBuffer vertices, float width, int color1, float colorLen, int color2) {
        draw(mvpMatrix, vertices, width, colorLen,
                ((float) Color.red(color1) * COLOR_CONV),
                ((float) Color.green(color1) * COLOR_CONV),
                ((float) Color.blue(color1) * COLOR_CONV),
                ((float) Color.alpha(color1) * COLOR_CONV),
                ((float) Color.red(color2) * COLOR_CONV),
                ((float) Color.green(color2) * COLOR_CONV),
                ((float) Color.blue(color2) * COLOR_CONV),
                ((float) Color.alpha(color2) * COLOR_CONV));
    }

    public void draw(float[] mvpMatrix, FloatBuffer vertices, float width, float colorLen,
                     float r1, float g1, float b1, float a1, float r2, float g2, float b2, float a2) {

        // Add program to OpenGL environment
        GLES20.glUseProgram(mProgram);

        GLES20.glEnableVertexAttribArray(mPositionParam);

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mMVPParam, 1, false, mvpMatrix, 0);

        GLES20.glLineWidth(width);

        float[] color1 = {r1, g1, b1, a1};
        float[] color2 = {r2, g2, b2, a2};
        GLES20.glUniform4fv(mColorParam, 1, color1, 0);
        GLES20.glUniform4fv(mColorAltParam, 1, color2, 0);
        GLES20.glUniform1f(mColorLenParam, colorLen);

        vertices.position(0);
        int numVertices = vertices.remaining() / (COORDS_PER_VERTEX);
        GLES20.glVertexAttribPointer(mPositionParam, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false, 0, vertices);

        GLES20.glDrawArrays(GLES20.GL_LINES, 0, numVertices);

        GLES20.glDisableVertexAttribArray(mPositionParam);

    }
}
