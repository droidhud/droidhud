package org.fuster.droidhud;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.opengl.GLES20;
import android.util.Log;
import android.view.Surface;

import com.android.grafika.MoviePlayer;
import com.android.grafika.SpeedControlCallback;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLVideo implements
        SurfaceTexture.OnFrameAvailableListener,
        MoviePlayer.PlayerFeedback,
        MoviePlayer.FrameCallback
{

    public static final String TAG = "GLVideo";

    private static final int FLOAT_SIZE_BYTES = 4;
    private static final int TRIANGLE_VERTICES_DATA_STRIDE_BYTES = 5 * FLOAT_SIZE_BYTES;
    private static final int TRIANGLE_VERTICES_DATA_POS_OFFSET = 0;
    private static final int TRIANGLE_VERTICES_DATA_UV_OFFSET = 3;
    private final float[] GLVIDEO_TRIS = {
            // X, Y, Z, U, V
            -1.0f, -1.0f, 0, 0.f, 0.f,
            1.0f, -1.0f, 0, 1.f, 0.f,
            -1.0f,  1.0f, 0, 0.f, 1.f,
            1.0f,  1.0f, 0, 1.f, 1.f,
    };

    private static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    private FloatBuffer mTriangleVertices;

    //private float[] mMVPMatrix = new float[16];
    private float[] mSTMatrix = new float[16];
    private float[] mOldSTMatrix = new float[16];

    private int mProgram;
    private int mTextureID;
    private int muMVPMatrixHandle;
    private int muSTMatrixHandle;
    private int maPositionHandle;
    private int maTextureHandle;

    private SurfaceTexture mSurfaceTexture;
    private Surface mSurface;
    private boolean updateSurface = false;
    private MoviePlayer.PlayTask mPlayTask = null;
    private MoviePlayer mPlayer = null;
    private HUDActivity mActivity = null;

    public GLVideo(Context context, HUDActivity activity)
    {
        mActivity = activity;
        mActivity.getCardboardView().setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        mTriangleVertices = ByteBuffer.allocateDirect(
                GLVIDEO_TRIS.length * FLOAT_SIZE_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(GLVIDEO_TRIS).position(0);


        Resources res = context.getResources();

        int vertexShader = GLUtils.loadGLShader(TAG, res, GLES20.GL_VERTEX_SHADER, R.raw.glvideo_vertex);
        int fragmentShader = GLUtils.loadGLShader(TAG, res, GLES20.GL_FRAGMENT_SHADER, R.raw.glvideo_fragment);
        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);
        GLES20.glUseProgram(mProgram);
        if (mProgram == 0) {
            return;
        }

        GLUtils.checkGLError(TAG, "program");

        Matrix.setIdentityM(mSTMatrix, 0);

        maPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        GLUtils.checkGLError(TAG, "glGetAttribLocation aPosition");
        if (maPositionHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aPosition");
        }
        maTextureHandle = GLES20.glGetAttribLocation(mProgram, "aTextureCoord");
        GLUtils.checkGLError(TAG, "glGetAttribLocation aTextureCoord");
        if (maTextureHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aTextureCoord");
        }

        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLUtils.checkGLError(TAG, "glGetUniformLocation uMVPMatrix");
        if (muMVPMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uMVPMatrix");
        }

        muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uSTMatrix");
        GLUtils.checkGLError(TAG, "glGetUniformLocation uSTMatrix");
        if (muSTMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uSTMatrix");
        }

        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);

        mTextureID = textures[0];
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, mTextureID);
        GLUtils.checkGLError(TAG, "glBindTexture mTextureID");

        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER,
                GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER,
                GLES20.GL_LINEAR);


        mSurfaceTexture = new SurfaceTexture(mTextureID);
        mSurfaceTexture.setOnFrameAvailableListener(this);

        mSurface = new Surface(mSurfaceTexture);
    }

    public void stop()
    {
        if (null != mPlayer)
        {
            mPlayer.requestStop();
        }
    }

    public void playFile(String filename)
    {
        // only needed when not playing back live streams
        SpeedControlCallback callback = new SpeedControlCallback();

        try {
            mPlayer = new MoviePlayer(
                    new File(filename), mSurface, callback);
        } catch (IOException ioe) {
            Log.e(TAG, "Unable to play movie", ioe);
            mSurface.release();
            mPlayer = null;
            return;
        }

        mPlayTask = new MoviePlayer.PlayTask(mPlayer, this);
        mPlayTask.setLoopMode(true);
        mPlayTask.execute();
    }

    public void draw(float[] mvpMatrix) {

        if (null == mPlayer) return;

        synchronized(this) {
            if (updateSurface) {
                mSurfaceTexture.updateTexImage();
                mSurfaceTexture.getTransformMatrix(mSTMatrix);
                /*
                if (!mSTMatrix.equals(mOldSTMatrix))
                {
                    Log.d(TAG, "New texture matrix: " + mSTMatrix.toString());
                    mOldSTMatrix = mSTMatrix.clone();
                }
                */
                updateSurface = false;
            }
        }

        GLES20.glUseProgram(mProgram);
        GLUtils.checkGLError(TAG, "glUseProgram");

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, mTextureID);

        mTriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
        GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false,
                TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
        GLUtils.checkGLError(TAG, "glVertexAttribPointer maPosition");
        GLES20.glEnableVertexAttribArray(maPositionHandle);
        GLUtils.checkGLError(TAG, "glEnableVertexAttribArray maPositionHandle");

        mTriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
        GLES20.glVertexAttribPointer(maTextureHandle, 3, GLES20.GL_FLOAT, false,
                TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
        GLUtils.checkGLError(TAG, "glVertexAttribPointer maTextureHandle");
        GLES20.glEnableVertexAttribArray(maTextureHandle);
        GLUtils.checkGLError(TAG, "glEnableVertexAttribArray maTextureHandle");

        //Matrix.setIdentityM(mvpMatrix, 0);
        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mvpMatrix, 0);
        GLES20.glUniformMatrix4fv(muSTMatrixHandle, 1, false, mSTMatrix, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLUtils.checkGLError(TAG, "glDrawArrays");
        GLES20.glFinish();
    }

    synchronized public void onFrameAvailable(SurfaceTexture surface) {
        updateSurface = true;
    }

    @Override
    public void preRender(long presentationTimeUsec) {
        // We let the source set the pace since we are using a live stream
    }

    @Override
    public void postRender() {

    }

    @Override
    public void loopReset() {

    }

    @Override
    public void playbackStopped() {
        Log.d(TAG, "playbackStopped()");
    }
}
