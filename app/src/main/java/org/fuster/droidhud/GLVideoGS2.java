package org.fuster.droidhud;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.LinkedList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.widget.Toast;
import android.os.HandlerThread;
import android.os.Process;

import org.freedesktop.gstreamer.GStreamer;


public class GLVideoGS2 extends MediaCodec.Callback implements SurfaceTexture.OnFrameAvailableListener {

    public static final String TAG = "GLVideoGS2";

    private static final int FLOAT_SIZE_BYTES = 4;
    private static final int TRIANGLE_VERTICES_DATA_STRIDE_BYTES = 5 * FLOAT_SIZE_BYTES;
    private static final int TRIANGLE_VERTICES_DATA_POS_OFFSET = 0;
    private static final int TRIANGLE_VERTICES_DATA_UV_OFFSET = 3;
    private final float[] GLVIDEO_TRIS = {
            // X, Y, Z, U, V
            -1.0f, -1.0f, 0, 0.f, 0.f,
            1.0f, -1.0f, 0, 1.f, 0.f,
            -1.0f,  1.0f, 0, 0.f, 1.f,
            1.0f,  1.0f, 0, 1.f, 1.f,
    };

    private static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    private FloatBuffer mTriangleVertices;

    //private float[] mMVPMatrix = new float[16];
    private float[] mSTMatrix = new float[16];

    private int mProgram;
    private int mTextureID;
    private int muMVPMatrixHandle;
    private int muSTMatrixHandle;
    private int maPositionHandle;
    private int maTextureHandle;

    private HandlerThread mCodecThread;
    private SurfaceTexture mSurfaceTexture;
    private Surface mSurface;
    private boolean updateTexture = false;
    private boolean mNativeInitialized = false;
    private LinkedList<Integer> mInputBuffers;

    private native void nativeInit(String pipeline);
    private native void nativeFinalize(); // Destroy pipeline and shutdown native code
    private native void nativePlay();     // Set pipeline to PLAYING
    private native void nativePause();    // Set pipeline to PAUSED
    private static native boolean nativeClassInit(); // Initialize native class: cache Method IDs for callbacks
    private long native_custom_data;

    private MediaCodec mCodec;
    private MediaFormat mMediaFormat;
    private Handler mCodecHandler;
    private boolean mFirstFrameReceived = false;
    private boolean mRenderContinuously;

    private long mFrameCount = 0;
    private long mLastFPSTimeNs = 0;
    public float FPS = 0.0f;
    private HUDActivity mActivity;
    public VideoWatchdog mVideoWatchdog = null;

    //private static String appsinkCaps = "caps=video/x-h264";
/*    private static String defaultPipelineDescription = "udpsrc port=%d " +
            "caps=application/x-rtp,media=(string)video,clock-rate=(int)90000,encoding-name=(string)H264 " +
            "! rtph264depay ! h264parse " +
            "! appsink name=sink sync=false emit-signals=true max-buffers=2 drop=true " + appsinkCaps;*/

    private static String appsinkCaps = "";
    private static String defaultPipelineDescription = "udpsrc port=%d " +
            "caps=application/x-rtp,media=(string)video,clock-rate=(int)90000,encoding-name=(string)H264 " +
            "! rtph264depay ! h264parse ! amcviddec-omxgoogleh264decoder name=decoder " +
            "! appsink name=sink sync=false max-buffers=2 drop=true " + appsinkCaps;

    static {
        System.loadLibrary("gstreamer_android");
        System.loadLibrary("glvideogs2");
        nativeClassInit();
    }

    public GLVideoGS2(Context context, int udpPort, HUDActivity activity, boolean renderContinuously)
    {
        this(context, String.format(defaultPipelineDescription, udpPort), activity, renderContinuously);
    }

    public GLVideoGS2(Context context, String pipelineDescription, HUDActivity activity, boolean renderContinuously)
    {
        mActivity = activity;
        if (renderContinuously) {
            mActivity.getCardboardView().setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        } else {
            mActivity.getCardboardView().setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        }
        mRenderContinuously = renderContinuously;

        mTriangleVertices = ByteBuffer.allocateDirect(
                GLVIDEO_TRIS.length * FLOAT_SIZE_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(GLVIDEO_TRIS).position(0);


        Resources res = context.getResources();

        int vertexShader = GLUtils.loadGLShader(TAG, res, GLES20.GL_VERTEX_SHADER, R.raw.glvideo_vertex);
        int fragmentShader = GLUtils.loadGLShader(TAG, res, GLES20.GL_FRAGMENT_SHADER, R.raw.glvideo_fragment);
        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);
        GLES20.glUseProgram(mProgram);
        if (mProgram == 0) {
            return;
        }

        GLUtils.checkGLError(TAG, "program");

        Matrix.setIdentityM(mSTMatrix, 0);

        maPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        GLUtils.checkGLError(TAG, "glGetAttribLocation aPosition");
        if (maPositionHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aPosition");
        }
        maTextureHandle = GLES20.glGetAttribLocation(mProgram, "aTextureCoord");
        GLUtils.checkGLError(TAG, "glGetAttribLocation aTextureCoord");
        if (maTextureHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aTextureCoord");
        }

        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLUtils.checkGLError(TAG, "glGetUniformLocation uMVPMatrix");
        if (muMVPMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uMVPMatrix");
        }

        muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uSTMatrix");
        GLUtils.checkGLError(TAG, "glGetUniformLocation uSTMatrix");
        if (muSTMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uSTMatrix");
        }

        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);

        mTextureID = textures[0];
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, mTextureID);
        GLUtils.checkGLError(TAG, "glBindTexture mTextureID");

        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER,
                GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER,
                GLES20.GL_LINEAR);

        mSurfaceTexture = new SurfaceTexture(mTextureID);
        mSurfaceTexture.setOnFrameAvailableListener(this);

        mSurface = new Surface(mSurfaceTexture);

        mInputBuffers = new LinkedList<Integer>();

        mCodecThread = new HandlerThread("CodecThread", Process.THREAD_PRIORITY_FOREGROUND);
        mCodecThread.start();
        mCodecHandler = new Handler(mCodecThread.getLooper());
        //mCodec.setCallback(this, codecHandler);
        //mCodec.start();

        // Initialize GStreamer and warn if it fails
        try {
            GStreamer.init(context);
            Log.i(TAG, "Finished GStreamer init");
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            onDestroy();
            return;
        }
        nativeInit(pipelineDescription);

        if (!renderContinuously) {
            mVideoWatchdog = new VideoWatchdog(new Handler(mActivity.getMainLooper()), 100, activity.getVideoFPS() * 2, 1000);
        }
    }

    void play() {
        if (null != mMediaFormat)
        {
            // TODO assumption is that media format is only provided by Gstreamer
            // once per pipeline creation and not once per play/pause
            mCodec = startCodec(mMediaFormat, mCodecHandler, mSurface);
            if (mCodec != null ) {
                Log.i(TAG, "MediaFormat reused and codec recreated!");
            }
        }

        if (mNativeInitialized) {
            nativePlay();
        }
        if (mVideoWatchdog != null) mVideoWatchdog.start();
    }

    void pause() {
        if (mVideoWatchdog != null) mVideoWatchdog.stop();

        if (mNativeInitialized) {
            Log.d(TAG, "Pausing native lib");
            nativePause();
        }

        stopCodec(mCodec);
        mCodec = null;
    }

    protected void finalize() {
        onDestroy();
    }

    public void onDestroy() {
        Log.i(TAG, "onDestroy()");
        nativeFinalize();
        mMediaFormat = null;
    }

    private MediaCodec startCodec(MediaFormat format, Handler codecHandler, Surface surface)
    {
        MediaCodec codec = null;
        MediaCodecList mcList = new MediaCodecList(MediaCodecList.ALL_CODECS);
        String decoderName = mcList.findDecoderForFormat(format);

        mFirstFrameReceived = false;
        Log.i(TAG, "startCodec(): Using decoder: " + decoderName);
        try {
            codec = MediaCodec.createByCodecName(decoderName);
            codec.setCallback(this, codecHandler);
            codec.configure(format, surface, null, 0);
            codec.start();
        }
        catch (Exception e) {
            Log.e(TAG, "startCodec(): " + e.toString());
            stopCodec(codec);
            codec = null;
        }

        return codec;
    }

    private void stopCodec(final MediaCodec codec)
    {
        if (null != codec) {
            Runnable stopCodecThread = new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d(TAG, "codec.stop()");
                        // TODO this hangs sometimes?
                        codec.stop();
                        Log.d(TAG, "codec.release()");
                        codec.release();
                        Log.d(TAG, "codec release complete");
                    }
                    catch (Exception e) {
                        Log.e(TAG, "Exception when stopping and releasing codec");
                    }
                }
            };
            new Thread(stopCodecThread).start();

        }
    }

    // Called from native code. This sets the content of the TextView from the UI thread.
    private void setMessage(final String message) {
        Log.i(TAG, "setMessage(): " + message);
    }

    // Called from native code. Native code calls this once it has created its pipeline and
    // the main loop is running, so it is ready to accept commands.
    private void onGStreamerInitialized () {
        Log.i(TAG, "GStreamer initialized:");
        mNativeInitialized = true;
        play();
    }

    public void draw(float[] mvpMatrix) {

        synchronized(this) {
            if (updateTexture) {
                mSurfaceTexture.updateTexImage();
                mSurfaceTexture.getTransformMatrix(mSTMatrix);
                updateTexture = false;
            }
        }

        GLES20.glUseProgram(mProgram);
        GLUtils.checkGLError(TAG, "glUseProgram");

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, mTextureID);
        GLUtils.checkGLError(TAG, "glBindTexture id: " + mTextureID);

        mTriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
        GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false,
                TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
        GLUtils.checkGLError(TAG, "glVertexAttribPointer maPosition");
        GLES20.glEnableVertexAttribArray(maPositionHandle);
        GLUtils.checkGLError(TAG, "glEnableVertexAttribArray maPositionHandle");

        mTriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
        GLES20.glVertexAttribPointer(maTextureHandle, 3, GLES20.GL_FLOAT, false,
                TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
        GLUtils.checkGLError(TAG, "glVertexAttribPointer maTextureHandle");
        GLES20.glEnableVertexAttribArray(maTextureHandle);
        GLUtils.checkGLError(TAG, "glEnableVertexAttribArray maTextureHandle");

        //Matrix.setIdentityM(mvpMatrix, 0);
        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mvpMatrix, 0);
        GLES20.glUniformMatrix4fv(muSTMatrixHandle, 1, false, mSTMatrix, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLUtils.checkGLError(TAG, "glDrawArrays");

        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, 0);
        // This seems to limit FPS dramatically
        //GLES20.glFinish();
    }

    /* Called by native code to get an input buffer from the codec */
    public int getCodecInputBufferIndex(boolean isKeyFrame) {
        ByteBuffer bb;
        int bufferIdx = -1;

        //Log.i(TAG, "getCodecInputBufferIndex() called!");

        //if (isKeyFrame) {
            //Log.d(TAG, "getCodecInputBufferIndex(): key frame");
        //}

        if (mCodec == null) {
            Log.w(TAG, "getCodecInputBufferIndex(): Codec not yet configured");
        } else {
            if (!mFirstFrameReceived) {
                // Wait until we have a buffer free since apparently we can't miss the
                // first frame or the codec fails
                while (mInputBuffers.size() == 0) {
                    try {
                        Log.d(TAG, "Waiting for a buffer for first frame");
                        Thread.sleep(100, 0);
                    } catch (Exception e) {
                        Log.e(TAG, "getCodecInputBufferIndex(): Failed waiting for a buffer");
                        break;
                    }
                }
            }

            if (mInputBuffers.size() == 0) {
                Log.e(TAG, "getCodecInputBufferIndex(): No input buffers available! Dropping encoded frame");
            } else {
                bufferIdx = mInputBuffers.removeFirst();
                if (!mFirstFrameReceived) {
                    Log.d(TAG, "First frame for codec");
                    mFirstFrameReceived = true;
                }
            }
        }

        return bufferIdx;
    }

    /* Called by native code to change a codec buffer index into a ByteBuffer */
    public ByteBuffer getCodecInputBuffer(int bufferIdx) {
        //Log.i(TAG, "getCodecInputBuffer() called!");
        return mCodec.getInputBuffer(bufferIdx);
    }

    private void updateFPS()
    {
        long timeNs = System.nanoTime();
        // FPS
        mFrameCount++;
        long diffTime = timeNs - mLastFPSTimeNs;
        if (diffTime >= 1000000000) {
            FPS = (float)mFrameCount / ((float)diffTime * 1e-9f);
            mFrameCount = 0;
            mLastFPSTimeNs = timeNs;
        }
    }

    /* Called by native code as it receives data via appdecodersink */
    public void onEncodedFrameAvailable(int bufferIdx, int size) {
        //Log.i(TAG, "onEncodedFrameAvailable() called!");

        // According to http://developer.android.com/reference/android/media/MediaCodec.html#releaseOutputBuffer%28int,%20long%29
        // setting the presentation time to something more than one second different than the current time
        // will cause the Surface to just render the frame as soon as possible
        mCodec.queueInputBuffer(bufferIdx, 0, size, 0, 0);
    }

    /* Called by native code when it creates MediaFormat after receiving codec data */
    public void onMediaFormatAvailable(MediaFormat format) {
        Log.i(TAG, "onMediaFormatAvailable() called!");
        if (mCodec != null) {
            Log.e(TAG, "mCodec is not null! Releasing existing codec first...");
            stopCodec(mCodec);
            mCodec = null;
        }

        mMediaFormat = format;
        mCodec = startCodec(mMediaFormat, mCodecHandler, mSurface);
        if (mCodec != null ) {
            Log.i(TAG, "MediaFormat created and codec started!");
        }
    }

    @Override
    public void onInputBufferAvailable(MediaCodec codec, int index) {
        //Log.i(TAG, "onInputBufferAvailable()");
        mInputBuffers.add(index);
    }

    @Override
    public void onOutputBufferAvailable(MediaCodec codec, int index, MediaCodec.BufferInfo info) {
        // Only need to release the buffer immediately
        // The surface will automatically be updated
        boolean doRender = (info.size != 0);
        //Log.i(TAG, "onOutputBufferAvailable(): doRender: " + doRender);
        codec.releaseOutputBuffer(index, doRender);
    }

    synchronized public void onFrameAvailable(SurfaceTexture surface) {
        updateTexture = true;
        // Immediately request a render
        //Log.i(TAG, "onFrameAvailable()");
        if (!mRenderContinuously) {
            mActivity.getCardboardView().requestRender();
        }
        if (mVideoWatchdog != null) {
            mVideoWatchdog.tickle();
        }
        updateFPS();

    }

    @Override
    public void onError(MediaCodec codec, MediaCodec.CodecException e) {
        // parse error type and respond accordingly
        Log.e(TAG, "MediaCodec exception: " + e.toString());
        if (!e.isTransient() && !e.isRecoverable())
        {
            // TODO
            Log.e(TAG, "MediaCodec error is fatal!");
        }
        else if (e.isRecoverable())
        {
            // TODO
            Log.e(TAG, "MediaCodec error is recoverable!");
        }
    }

    @Override
    public void onOutputFormatChanged(MediaCodec codec, MediaFormat format) {
        Log.i(TAG, "MediaCodec output format change: " + format.toString());
    }

    public class VideoWatchdog implements Runnable {
        long lastTickleTimeNs;
        int numTickles;
        int numReenableTickles;
        int intervalMs;
        long timeoutNs;
        boolean stop = false;
        Handler handler;

        public VideoWatchdog(Handler handler, final int intervalMs, final int numReenableTickles, final int timeoutMs) {
            lastTickleTimeNs = 0;
            numTickles = 0;
            this.intervalMs = intervalMs;
            this.numReenableTickles = numReenableTickles;
            timeoutNs = (long)timeoutMs * 1000000;
            this.handler = handler;
        }

        public void start() {
            stop = false;
            lastTickleTimeNs = 0;
            numTickles = 0;
            handler.postDelayed(this, intervalMs);
        }

        public void stop() {
            stop = true;
        }

        public void tickle() {
            if (!stop) {
                lastTickleTimeNs = System.nanoTime();
                numTickles++;
            }
        }

        @Override
        public void run() {
            if (stop) return;

            handler.postDelayed(this, intervalMs);

            long timeNs = System.nanoTime();
            long diffTime = timeNs - lastTickleTimeNs;
            if (diffTime >= timeoutNs && lastTickleTimeNs > 0) {
                numTickles = 0;
                if (mActivity.getCardboardView().getRenderMode() == GLSurfaceView.RENDERMODE_WHEN_DIRTY) {
                    Log.e(TAG, "No video for " + (diffTime / 1000000) + " ms, switching to continuous rendering");
                    mActivity.getCardboardView().setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
                    FPS = 0.0f;
                }
            } else if (numTickles > numReenableTickles)
                if (mActivity.getCardboardView().getRenderMode() == GLSurfaceView.RENDERMODE_CONTINUOUSLY) {
                    Log.i(TAG, "Re-enabling render-when-dirty");
                    mActivity.getCardboardView().setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
                }
        }
    }
}
