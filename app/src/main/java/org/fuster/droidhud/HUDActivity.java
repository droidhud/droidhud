package org.fuster.droidhud;

import java.nio.FloatBuffer;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;

import com.google.vrtoolkit.cardboard.CardboardActivity;
import com.google.vrtoolkit.cardboard.CardboardView;
import com.google.vrtoolkit.cardboard.Eye;
import com.google.vrtoolkit.cardboard.HeadTransform;
import com.google.vrtoolkit.cardboard.Viewport;

import javax.microedition.khronos.egl.EGLConfig;

import com.android.texample2.GLText;


public class HUDActivity extends CardboardActivity implements CardboardView.StereoRenderer {

    public static String TAG = "HUDActivity";

    private static final float Z_NEAR = 0.1f;
    private static final float Z_FAR = 100.0f;

    private static final float CAMERA_Z = 0.01f;

    private static final int MAX_LINES = 48;

    public static final int VIDEO_RESOLUTION_320x240 = 0;
    public static final int VIDEO_RESOLUTION_640x480 = 1;
    public static final int VIDEO_RESOLUTION_960x540 = 2;

    private static final String DEFAULT_TELEMETRY_UDP_PORT = "1235";
    private static final String DEFAULT_VIDEO_UDP_PORT = "1235";
    private static final int DISCOVERY_UDP_PORT = 1236;

    private static final double ATTITUDE_FOV_DEG = 4.0;
    private static final float ATTITUDE_MODEL_DISTANCE = 90f;
    private static final float VIDEO_MODEL_DISTANCE = 90f;
    private static final String DEFAULT_FPV_CAMERA_ELEVATION = "0.0f";
    private static final String DEFAULT_CAMERA_VERT_FOV_DEG = "90.0f";
    private static final String DEFAULT_HUD_COLOR = "ff00ff00";
    private static final String DEFAULT_HUD_ALERT_COLOR = "ffff0000";
    private static final String DEFAULT_PITCH_LADDER_STEP_DEG = "5";
    private static final String DEFAULT_VIDEO_FPS = "40";
    private static final String DEFAULT_VIDEO_RESOLUTION = "0";
    private static final String DEFAULT_VIDEO_BITRATE = "10000000";
    private static final String DEFAULT_TELEMETRY_FREQ = "20";
    private static final String DEFAULT_TELEMETRY_DIVISOR = "5";
    private static final String DEFAULT_LOW_BATTERY_THRESHOLD = "14.6";

    private float mFPVCameraElevation;
    private float mCameraFOV;
    private int mHUDColor;
    private int mHUDAlertColor;
    private int mPitchLadderSteps;
    private int mVideoFPS;
    private int mVideoWidth;
    private int mVideoHeight;
    private int mVideoBitrate;
    private int mTelemetryFreq;
    private int mTelemetryDivisor;
    private float mVbatThreshold;
    private long mAlertPeriodNs = 2000000000;
    private boolean mEnableHeadTracking;

    private FloatBuffer fbPitchLadderPos;
    private FloatBuffer fbPitchLadderNeg;
    private FloatBuffer fbLevelPosInd;
    private FloatBuffer fbBoxes;


    private int mWidth = 0;
    private int mHeight = 0;
    private float[] viewHead = new float[16];
    private float[] attitudeCamera = new float[16];
    private float[] viewAttitude = new float[16];
    private float[] viewVideo = new float[16];
    private float[] viewHeadFixed = new float[16];
    private float[] rotateMatrix = new float[16];
    private float[] modelPitchLadderPosOrig = new float[16];
    private float[] modelPitchLadderNegOrig = new float[16];
    private float[] modelPitchLadderPos = new float[16];
    private float[] modelPitchLadderNeg = new float[16];
    private float[] modelLevelPosInd = new float[16];
    private float[] modelBoxes = new float[16];
    private float[] modelVideo = new float[16];
    private float[] perspectiveHUDAttitude = new float[16];
    private float[] perspectiveHUDFixed = new float[16];
    private float[] viewHUDFixed = new float[16];
    private float[] vpHUDFixed = new float[16];
    private float[] mvpTemp = new float[16];
    private float[] mvTemp = new float[16];
    private float[] mvpVideo = new float[16];

    private GLText glText;                             // A GLText Instance
    private GLLine glLine;
    private GLVideo glVideo;
    private GLVideoGS2 glVideoGS;
    private Context context;                           // Context (from Activity)

    private Telemetry telemetry;
    private boolean mFirstRun = true;
    private boolean mUseDummyTelemetry = false;
    private int mVideoUDPPort;
    private boolean mUseLocalVideo = false;
    private String mLocalVideoFile;
    private boolean mRenderContinuously = false;

    private long mFrameCount = 0;
    private long mLastFPSTimeNs = 0;
    private float mFPS = 0.0f;
    private VehicleDiscovery mVehicleDiscovery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();

        int telemUDPPort = Integer.valueOf(DroidHUDApp.mPrefs.getString("telemetry_udp_port", DEFAULT_TELEMETRY_UDP_PORT));
        mVideoUDPPort = Integer.valueOf(DroidHUDApp.mPrefs.getString("video_udp_port", DEFAULT_VIDEO_UDP_PORT));
        mUseDummyTelemetry = DroidHUDApp.mPrefs.getBoolean("dbg_dummy_telemetry", false);
        mUseLocalVideo = DroidHUDApp.mPrefs.getBoolean("dbg_use_video_file", false);
        mLocalVideoFile = DroidHUDApp.mPrefs.getString("dbg_video_file", "");
        mCameraFOV = Float.valueOf(DroidHUDApp.mPrefs.getString("camera_fov", DEFAULT_CAMERA_VERT_FOV_DEG));
        mTelemetryFreq = Integer.valueOf(DroidHUDApp.mPrefs.getString("telemetry_fps", DEFAULT_TELEMETRY_FREQ));
        mTelemetryDivisor = Integer.valueOf(DroidHUDApp.mPrefs.getString("telemetry_divisor", DEFAULT_TELEMETRY_DIVISOR));
        mVideoFPS = Integer.valueOf(DroidHUDApp.mPrefs.getString("video_fps", DEFAULT_VIDEO_FPS));
        mVideoBitrate = Integer.valueOf(DroidHUDApp.mPrefs.getString("video_bitrate", DEFAULT_VIDEO_BITRATE));
        int videoResolution = Integer.valueOf(DroidHUDApp.mPrefs.getString("video_resolution", DEFAULT_VIDEO_RESOLUTION));
        switch (videoResolution)
        {
            case VIDEO_RESOLUTION_960x540:
                mVideoWidth = 960;
                mVideoHeight = 540;
                break;
            case VIDEO_RESOLUTION_640x480:
                mVideoWidth = 640;
                mVideoHeight = 480;
                break;
            case VIDEO_RESOLUTION_320x240:
            default:
                mVideoWidth = 320;
                mVideoHeight = 240;
                break;
        }
        mFPVCameraElevation = Float.valueOf(DroidHUDApp.mPrefs.getString("camera_elevation", DEFAULT_FPV_CAMERA_ELEVATION));
        mHUDColor = (int)Long.parseLong(DroidHUDApp.mPrefs.getString("hud_color", DEFAULT_HUD_COLOR), 16);
        mHUDAlertColor = (int)Long.parseLong(DroidHUDApp.mPrefs.getString("hud_alert_color", DEFAULT_HUD_ALERT_COLOR), 16);
        mPitchLadderSteps = Integer.parseInt(DroidHUDApp.mPrefs.getString("pitch_ladder_step_deg", DEFAULT_PITCH_LADDER_STEP_DEG));
        mVbatThreshold = Float.valueOf(DroidHUDApp.mPrefs.getString("low_battery_threshold", DEFAULT_LOW_BATTERY_THRESHOLD));
        mEnableHeadTracking = DroidHUDApp.mPrefs.getBoolean("enable_head_tracking", false);
        mRenderContinuously = DroidHUDApp.mPrefs.getBoolean("dbg_render_continuously", false);

        Log.i(TAG, String.format("FOV: %f, Elev: %f, HUDColor: 0x%08X", mCameraFOV, mFPVCameraElevation, mHUDColor));

        // Start telemetry thread
        telemetry = new Telemetry(telemUDPPort);

        setContentView(R.layout.activity_hud);
        CardboardView cardboardView = (CardboardView) findViewById(R.id.cardboard_view);
        cardboardView.setRestoreGLStateEnabled(false);
        cardboardView.setRenderer(this);
        setCardboardView(cardboardView);

        cardboardView.setElectronicDisplayStabilizationEnabled(mEnableHeadTracking);

        //fbPitchLadderPos = GLLine.initFloatBuffer(2+4);
        fbLevelPosInd = GLLine.initFloatBuffer(2);
        fbBoxes = GLLine.initFloatBuffer(8);

        mVehicleDiscovery = new VehicleDiscovery(DISCOVERY_UDP_PORT, mVideoUDPPort, telemUDPPort);
        mVehicleDiscovery.setConfig(mTelemetryFreq, mTelemetryDivisor, mVideoFPS, mVideoWidth, mVideoHeight, mVideoBitrate);

    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Set WiFi for max performace
        Utils.keepWiFiOn(getApplicationContext(), true);

        if (!mUseDummyTelemetry) {
            telemetry.startListening();
        }

        if (null != glVideoGS) {
            glVideoGS.play();
        }

        if (mVehicleDiscovery != null) {
            mVehicleDiscovery.startDiscovery();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if (mVehicleDiscovery != null) {
            mVehicleDiscovery.stopDiscovery();
        }

        if (null != glVideoGS) {
            glVideoGS.pause();
        }

        if (null != glVideo) {
            glVideo.stop();
            mFirstRun = false;
        }

        Utils.keepWiFiOn(getApplicationContext(), false);

        telemetry.stopListening();

    }

    @Override
    public void onDestroy()
    {
        if (null != glVideoGS) {
            glVideoGS.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onRendererShutdown() {
        Log.i(TAG, "onRendererShutdown");
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        Log.i(TAG, "onSurfaceChanged: " + width + "x" + height);
        //glVideoGS.onSurfaceChanged();
        GLES20.glViewport(0, 0, width, height);
    }

    /**
     * Creates the buffers we use to store information about the 3D world.
     *
     * <p>OpenGL doesn't use Java arrays, but rather needs data in a format it can understand.
     * Hence we use ByteBuffers.
     *
     * @param config The EGL configuration used when creating the surface.
     */
    @Override
    public void onSurfaceCreated(EGLConfig config) {
        Log.i(TAG, "onSurfaceCreated");

        //GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        glText = new GLText(context.getAssets());
        //glText.load( "RobotoMono-Medium.ttf", 24, 0, 0 );
        //glText.load("unispace bd.ttf", 24, 0, 0);
        glText.load("OCRA.ttf", 24, 0, 0);
        glText.setScale(1.0f);
        //glText.setSpace(6.0f);

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Dark background so text shows up well.

        glLine = new GLLine(getResources(), MAX_LINES);


        // Adjust the attitudeCamera for the FPV camera elevation
        float elevationRads = (float)Math.toRadians(mFPVCameraElevation);
        float centerY = (float)Math.sin(elevationRads);
        float centerZ = -(float)Math.cos(elevationRads);
        float upY = -centerZ;
        float upZ = -centerY;
        float cameraY = -CAMERA_Z * centerY;
        float cameraZ = -CAMERA_Z * centerZ;
        Matrix.setLookAtM(attitudeCamera, 0, 0.0f, cameraY, cameraZ, 0.0f, centerY, centerZ, 0.0f, upY, upZ);

        // Scale for ATTITUDE_MODEL_DISTANCE so we maintain a constant apparent size for the attitude
        // indicators
        float attitudeScale = (float)Math.tan(Math.toRadians(ATTITUDE_FOV_DEG)) * ATTITUDE_MODEL_DISTANCE;
        Matrix.setIdentityM(modelPitchLadderPosOrig, 0);
        Matrix.setIdentityM(modelPitchLadderNegOrig, 0);

        Matrix.setIdentityM(modelLevelPosInd, 0);
        Matrix.scaleM(modelLevelPosInd, 0, attitudeScale, attitudeScale, ATTITUDE_MODEL_DISTANCE);

        Matrix.setIdentityM(modelBoxes, 0);

        int numPitchLinesPerHemi = (int)Math.floor(80.0f / mPitchLadderSteps);

        fbPitchLadderPos = HUDLayoutData.getPitchLadderPos((float) mPitchLadderSteps, numPitchLinesPerHemi, attitudeScale, ATTITUDE_MODEL_DISTANCE);
        fbPitchLadderNeg = HUDLayoutData.getPitchLadderNeg((float)mPitchLadderSteps, numPitchLinesPerHemi, attitudeScale, ATTITUDE_MODEL_DISTANCE);
        fbLevelPosInd.put(HUDLayoutData.LEVEL_INDICATOR);
        fbBoxes.put(HUDLayoutData.BOXES);

        Matrix.setIdentityM(modelVideo, 0);

        // TODO need to normalize this for the perspective
        float videoScale = 40.0f;
        float videoModelDistance = 80.0f;
        Matrix.scaleM(modelVideo, 0, videoScale, videoScale, 1.0f);
        Matrix.translateM(modelVideo, 0, 0.0f, 0.0f, -videoModelDistance);

        if (mUseLocalVideo) {
            glVideo = new GLVideo(getApplicationContext(), this);
        } else {
            glVideoGS = new GLVideoGS2(getApplicationContext(), mVideoUDPPort, this, mRenderContinuously);
        }
        Matrix.setIdentityM(viewHeadFixed, 0);

        GLUtils.checkGLError(TAG, "onSurfaceCreated");
    }

    /**
     * Helper function to calculate FPS
     */
    private void updateFPS()
    {
        long timeNs = System.nanoTime();
        // FPS
        mFrameCount++;
        long diffTime = timeNs - mLastFPSTimeNs;
        if (diffTime >= 1000000000) {
            mFPS = (float)mFrameCount / ((float)diffTime * 1e-9f);
            mFrameCount = 0;
            mLastFPSTimeNs = timeNs;
        }
    }

    /**
     * Prepares OpenGL ES before we draw a frame.
     *
     * @param headTransform The head transformation in the new frame.
     */
    @Override
    public void onNewFrame(HeadTransform headTransform) {

        float[] view;

        if (mUseDummyTelemetry) {
            telemetry.updateDummyData();
        }

        Matrix.setRotateEulerM(rotateMatrix, 0, -telemetry.Pitch, 0, -telemetry.Roll);
        Matrix.multiplyMM(modelPitchLadderPos, 0, rotateMatrix, 0, modelPitchLadderPosOrig, 0);
        Matrix.multiplyMM(modelPitchLadderNeg, 0, rotateMatrix, 0, modelPitchLadderNegOrig, 0);

        // Head tracking?
        if (mEnableHeadTracking) {
            headTransform.getHeadView(viewHead, 0);
            view = viewHead;
        } else {
            view = viewHeadFixed;
        }

        Matrix.multiplyMM(viewVideo, 0, view, 0, attitudeCamera, 0);
        Matrix.multiplyMM(viewAttitude, 0, view, 0, attitudeCamera, 0);

        GLUtils.checkGLError(TAG, "onReadyToDraw");

        updateFPS();
    }

    /**
     * Draws a frame for an eye.
     *
     * @param eye The eye to render. Includes all required transformations.
     */
    @Override
    public void onDrawEye(Eye eye) {
        float[] perspective;
        GLUtils.checkGLError(TAG, "onDrawEye");

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        GLUtils.checkGLError(TAG, "glClear");

        if (eye.getViewport().width != mWidth || eye.getViewport().height != mHeight) {
            mWidth = eye.getViewport().width;
            mHeight = eye.getViewport().height;

            Log.i(TAG, "Calculating HUD view and perspectives");
            updateHUDVP(mWidth, mHeight);
        }

        if (mEnableHeadTracking) {
            perspective = eye.getPerspective(Z_NEAR, Z_FAR);
            Matrix.multiplyMM(mvTemp, 0, viewVideo, 0, modelVideo, 0);
            Matrix.multiplyMM(mvpVideo, 0, perspective, 0, mvTemp, 0);
        } else {
            Matrix.setIdentityM(mvpVideo, 0);
        }

        if (mUseLocalVideo) {
            if (mFirstRun) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        glVideo.playFile(mLocalVideoFile);
                    }
                });
            }

            glVideo.draw(mvpVideo);
        } else {
            glVideoGS.draw(mvpVideo);
        }

        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glEnable(GLES20.GL_BLEND);

        drawHUDText(vpHUDFixed);

        drawHUDAttitude(viewAttitude, perspectiveHUDAttitude);
        drawHUDBoxes(viewHUDFixed, perspectiveHUDFixed);

        GLES20.glDisable(GLES20.GL_BLEND);


        mFirstRun = false;
    }

    @Override
    public void onFinishFrame(Viewport viewport) {
    }

    private void updateHUDVP(int width, int height) {
        float ratio = (float) width / height;
        float halfFOV = (float)(0.5*Math.toRadians(mCameraFOV));
        float top;
        float nearZ;
        float right;

        // Take into account device orientation
        if (width > height) {
            top = 1.0f;
            right = ratio;
        } else {
            top = (float)(1.0 / ratio);
            right = 1.0f;
        }
        nearZ = (float)(top / Math.tan(halfFOV));
        Matrix.frustumM(perspectiveHUDFixed, 0, -right, right, -top, top, 1, 100);
        Matrix.frustumM(perspectiveHUDAttitude, 0, -right, right, -top, top, nearZ, 100);

        int useForOrtho = Math.min(width, height);

        //TODO: Is this wrong?
        Matrix.orthoM(viewHUDFixed, 0,
                -useForOrtho / 2,
                useForOrtho / 2,
                -useForOrtho / 2,
                useForOrtho / 2, 0.1f, 100f);

        Matrix.multiplyMM(vpHUDFixed, 0, perspectiveHUDFixed, 0, viewHUDFixed, 0);
    }

    public void drawHUDText(float[] mvpMatrix) {
        String altitude;
        String roll;
        String pitch;
        String yaw;
        String heading;
        String Vbat;
        String altAGL;
        String vertVel;
        float accelMag;
        String G;
        String videoStats;
        String telemetryStats;
        String altHold;
        String attHold;
        boolean alertFrame;
        boolean alertVbat;
        boolean alertTelemetry;
        boolean alertVideo;

        accelMag = (float)Math.sqrt(telemetry.Acc[0]*telemetry.Acc[0] +
                telemetry.Acc[1]*telemetry.Acc[1] +
                telemetry.Acc[2]*telemetry.Acc[2]);

        roll = String.format("R%4.0f", telemetry.Roll);
        pitch = String.format("P%4.0f", telemetry.Pitch);
        if (telemetry.haveSensor(Telemetry.SENSOR_FLAG_MAG))
        {
            heading = String.format("H%3.0f", telemetry.Heading);
        } else {
            heading = String.format("Y%3.0f", telemetry.Yaw);
        }
        altitude = "-------";
        if (telemetry.haveSensor(Telemetry.SENSOR_FLAG_BARO)) {
            altitude = String.format("B%6.1f", telemetry.AltBaro);
        }
        // TODO do we even need this or will it get mixed into the other altitude?
        if (telemetry.haveSensor(Telemetry.SENSOR_FLAG_SONAR) &&
                telemetry.AltAGL < 3.0f && telemetry.AltAGL >= 0.0f) {
            altitude = String.format("G%6.1f", telemetry.AltAGL);
        }
        vertVel = String.format("%5.2f", telemetry.VertVel);
        Vbat = String.format("%4.1f V", telemetry.Vbat);
        G = String.format("%.1f G", accelMag);
        if (mUseLocalVideo) {
            videoStats = String.format("--/%2.0f", mFPS);
        } else {
            videoStats = String.format("%2.0f/%2.0f", glVideoGS.FPS, mFPS);
        }
        telemetryStats = String.format("%2.0f", telemetry.FPS);
        altHold = (telemetry.AltMode == Telemetry.ALT_HOLD_NONE) ? "" : "ALT";
        if (telemetry.AttMode == Telemetry.ATT_HOLD_ANGLE) {
            attHold = "ATT A";
        } else if (telemetry.AttMode == Telemetry.ATT_HOLD_HORIZON) {
            attHold = "ATT H";
        } else {
            attHold = "";
        }

        alertFrame = (System.nanoTime() % mAlertPeriodNs) < (mAlertPeriodNs / 2) ? true : false;
        alertVbat = (telemetry.Vbat <= mVbatThreshold) ? alertFrame : false;
        alertTelemetry = (telemetry.FPS <= 0.9f * mTelemetryFreq) ? alertFrame : false;
        if (mUseLocalVideo)
        {
            alertVideo = false;
        } else {
            alertVideo = (glVideoGS.FPS <= 0.9f * mVideoFPS) ? alertFrame : false;
        }

        /* Paint these using the regular color */
        glText.begin(mHUDColor, mvpMatrix);
        glText.draw(roll, -50f, -200f);
        glText.draw(pitch, -50f, -230f);
        glText.draw(heading, -50f, 230f);
        glText.draw(altitude, 200f, 0f);
        glText.draw(vertVel, 250f, -30f);
        glText.draw(G, -250f, 170f);
        glText.draw(attHold, -250f, 0f);
        glText.draw(altHold, -250f, -30f);

        if (!alertVbat) glText.draw(Vbat, 200f, -200f);
        if (!alertVideo) glText.draw(videoStats, -250f, -170f);
        if (!alertTelemetry) glText.draw(telemetryStats, -250f, -200f);

        glText.end();

        /* Paint these using the alert color on alert frames */
        if (alertVbat || alertVideo || alertTelemetry) {
            glText.begin(mHUDAlertColor, mvpMatrix);
            if (alertVbat) glText.draw(Vbat, 200f, -200f);
            if (alertVideo) glText.draw(videoStats, -250f, -170f);
            if (alertTelemetry) glText.draw(telemetryStats, -250f, -200f);
            glText.end();
        }


    }

    public void drawHUDBoxes(float[] vMatrix, float[] pMatrix) {

        Matrix.multiplyMM(mvTemp, 0, vMatrix, 0, modelBoxes, 0);
        Matrix.multiplyMM(mvpTemp, 0, pMatrix, 0, mvTemp, 0);
        glLine.draw(mvpTemp, fbBoxes, 2.0f, mHUDColor);

        GLUtils.checkGLError(TAG, "drawHUDBoxes");
    }

    public void drawHUDAttitude(float[] vMatrix, float[] pMatrix) {

        Matrix.multiplyMM(mvTemp, 0, vMatrix, 0, modelPitchLadderPos, 0);
        Matrix.multiplyMM(mvpTemp, 0, pMatrix, 0, mvTemp, 0);
        glLine.draw(mvpTemp, fbPitchLadderPos, 5.0f, mHUDColor);

        Matrix.multiplyMM(mvTemp, 0, vMatrix, 0, modelPitchLadderNeg, 0);
        Matrix.multiplyMM(mvpTemp, 0, pMatrix, 0, mvTemp, 0);
        glLine.draw(mvpTemp, fbPitchLadderNeg, 5.0f, mHUDColor, 0.3f, 0x00000000);

        Matrix.multiplyMM(mvTemp, 0, vMatrix, 0, modelLevelPosInd, 0);
        Matrix.multiplyMM(mvpTemp, 0, pMatrix, 0, mvTemp, 0);
        glLine.draw(mvpTemp, fbLevelPosInd, 5.0f, mHUDColor);

        GLUtils.checkGLError(TAG, "drawHUDAttitude");
    }

    public int getVideoFPS() {
        return mVideoFPS;
    }
}
