package org.fuster.droidhud;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


public class DroidHUDApp extends Application {
    private static final String TAG = "DroidHUDApp";

    public static SharedPreferences mPrefs;
    
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Creating instance");

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        
        // Set default values for user preferences
        PreferenceManager.setDefaultValues(this, R.xml.pref_debug, false);
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
    }
    
    public static DroidHUDApp getInstance(Activity a) {
        return (DroidHUDApp)a.getApplication();
    }
}
