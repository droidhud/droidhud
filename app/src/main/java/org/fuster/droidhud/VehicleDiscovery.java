package org.fuster.droidhud;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Enumeration;

/**
 * Sends out a broadcast packet to convince any nearby vehicles to send their telemetry
 * to this device
 */
public class VehicleDiscovery {
    private static String TAG = "VehicleDiscovery";

    public static final int MAGIC = 0xc0c0f00d;
    public static final int MSG_TYPE_CONNECT = 0x0000000c;
    public static final int MSG_TYPE_DISCO = 0x000000dc;

    private static final int DEFAULT_VIDEO_FPS = 40;
    private static final int DEFAULT_VIDEO_WIDTH = 320;
    private static final int DEFAULT_VIDEO_HEIGHT = 240;
    private static final int DEFAULT_VIDEO_BITRATE = 10000000;
    private static final int DEFAULT_TELEM_FREQ = 20;
    private static final int DEFAULT_TELEM_DIVISOR = 4;

    private int mDiscoveryPort;
    private int mVideoPort;
    private int mTelemetryPort;
    private int mTelemetryFreq;
    private int mTelemetryDivisor;
    private int mVideoFPS;
    private int mVideoWidth;
    private int mVideoHeight;
    private int mVideoBitrate;
    private InetAddress mAddress;
    private DiscoveryThread mDiscoveryThread = null;

    public VehicleDiscovery(int discoveryPort, int videoPort, int telemetryPort)
    {
        mDiscoveryPort = discoveryPort;
        mVideoPort = videoPort;
        mTelemetryPort = telemetryPort;
        mTelemetryFreq = DEFAULT_TELEM_FREQ;
        mTelemetryDivisor = DEFAULT_TELEM_DIVISOR;
        mVideoFPS = DEFAULT_VIDEO_FPS;
        mVideoWidth = DEFAULT_VIDEO_WIDTH;
        mVideoHeight = DEFAULT_VIDEO_HEIGHT;
        mVideoBitrate = DEFAULT_VIDEO_BITRATE;

        try {
            mAddress = getBroadcast();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void setConfig(int telemetryFreq, int telemetryDivisor, int videoFPS, int videoWidth, int videoHeight, int videoBitrate)
    {
        mTelemetryFreq = telemetryFreq;
        mTelemetryDivisor = telemetryDivisor;
        mVideoFPS = videoFPS;
        mVideoWidth = videoWidth;
        mVideoHeight = videoHeight;
        mVideoBitrate = videoBitrate;
    }

    public void startDiscovery()
    {
        if (null != mAddress) {
            Log.d(TAG, "Sending broadcast to " + mAddress.toString() + ":" + mDiscoveryPort);
            mDiscoveryThread = new DiscoveryThread("DiscoveryThread", mAddress, mDiscoveryPort);
            mDiscoveryThread.start();
        }
    }

    public void stopDiscovery()
    {
        if (null != mDiscoveryThread) {
            mDiscoveryThread.stopThread();
            mDiscoveryThread = null;
        }

        if (null == mAddress) {
            return;
        }

        new Thread(new Runnable() {
            public void run() {
                DatagramSocket senderSocket;
                DatagramPacket packet;

                ByteBuffer bb = ByteBuffer.allocate(128);
                bb.order(ByteOrder.BIG_ENDIAN); // network order
                bb.putInt(MAGIC);
                bb.putInt(MSG_TYPE_DISCO);
                bb.putShort((short)mVideoPort);
                bb.putShort((short)mTelemetryPort);

                try {
                    senderSocket = new DatagramSocket();
                    senderSocket.setBroadcast(true);
                } catch (SocketException e) {
                    Log.e(TAG, "Failed to open socket");
                    e.printStackTrace();
                    return;
                }

                packet = new DatagramPacket(bb.array(), bb.position(), mAddress, mDiscoveryPort);
                try {
                    senderSocket.send(packet);
                } catch (SocketException e ) {
                    Log.w(TAG, "Could not open socket!");
                } catch (IOException e) {
                    Log.w(TAG, "Could not send disconnect packet!");
                }

            }
        }).start();


    }

    private static InetAddress getBroadcast() throws SocketException {
        System.setProperty("java.net.preferIPv4Stack", "true");
        for (Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces(); niEnum.hasMoreElements();) {
            NetworkInterface ni = niEnum.nextElement();
            if (!ni.isLoopback()) {
                InetAddress broadcast;
                for (InterfaceAddress interfaceAddress : ni.getInterfaceAddresses()) {
                    Log.d(TAG, "Interface: " + ni.getDisplayName() + " address: " + interfaceAddress.toString());
                    broadcast = interfaceAddress.getBroadcast();
                    if (null != broadcast) {
                        return broadcast;
                    }
                }
            }
        }
        return null;
    }

    class DiscoveryThread extends Thread {
        private DatagramSocket senderSocket;
        private DatagramPacket packet;
        private int port;
        private InetAddress address;
        private boolean terminate;

        public DiscoveryThread(String name, InetAddress address, int port) {
            super(name);
            this.port = port;
            this.address = address;
        }

        @Override
        public void run() {

            try {
                senderSocket = new DatagramSocket();
                senderSocket.setBroadcast(true);
            } catch (SocketException e) {
                Log.e(TAG, "Failed to open socket");
                e.printStackTrace();
                return;
            }

            ByteBuffer bb = ByteBuffer.allocate(128);
            bb.order(ByteOrder.BIG_ENDIAN); // network order
            bb.putInt(MAGIC);
            bb.putInt(MSG_TYPE_CONNECT);
            bb.putShort((short)mVideoPort);
            bb.putShort((short)mTelemetryPort);
            bb.putShort((short)mVideoWidth);
            bb.putShort((short)mVideoHeight);
            bb.putShort((short)mVideoFPS);
            bb.putInt(mVideoBitrate);
            bb.putShort((short)mTelemetryFreq);
            bb.putShort((short)mTelemetryDivisor);

            terminate = false;
            while (!terminate) {
                packet = new DatagramPacket(bb.array(), bb.position(), address, port);
                try {
                    senderSocket.send(packet);
                } catch (IOException e) {
                    Log.w(TAG, "Could not send broadcast packet!");
                }

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    // assume terminate was set
                }
            } // while not terminated
            Log.d(TAG, "Closing socket");
            senderSocket.close();
        }

        public void stopThread() {
            terminate = true;
            try {
                join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
