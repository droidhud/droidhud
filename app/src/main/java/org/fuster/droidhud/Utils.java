package org.fuster.droidhud;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Created by Joel on 8/9/2015.
 */
public class Utils {
    static WifiManager.WifiLock wifiLock = null;
    public static final String TAG = "DHUtils";

    public static void keepWiFiOn(Context context, boolean on) {
        if (wifiLock == null) {
            WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wm != null) {
                wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, TAG);
                wifiLock.setReferenceCounted(true);
            }
        }
        if (wifiLock != null) { // May be null if wm is null
            if (on) {
                wifiLock.acquire();
                Log.d(TAG, "Acquired WiFi lock");
            } else if (wifiLock.isHeld()) {
                wifiLock.release();
                Log.d(TAG, "Released WiFi lock");
            }
        }
    }
}
