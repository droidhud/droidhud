package org.fuster.droidhud;

import android.opengl.Matrix;
import android.util.Log;

import java.nio.FloatBuffer;

public class HUDLayoutData {

    public static final String TAG = "HUDLayoutData";

    public static final float[] HORIZ_LINE_COORDS = {
            // X, Y, Z
            -1.2f, 0.0f, -1.0f,
            -0.2f, 0.0f, -1.0f,
            0.2f, 0.0f, -1.0f,
            1.2f, 0.0f, -1.0f,
    };

    public static final float[] PITCH_LINE_COORDS = {
            // X, Y, Z
            -1.0f, -0.15f, -1.0f,
            -1.0f, 0.0f, -1.0f,
            -1.0f, 0.0f, -1.0f,
            -0.6f, 0.0f, -1.0f,
            0.6f, 0.0f, -1.0f,
            1.0f, 0.0f, -1.0f,
            1.0f, 0.0f, -1.0f,
            1.0f, -0.15f, -1.0f,
    };

    public static final float[] LEVEL_INDICATOR = {
            // X, Y, Z
            -0.2f, 0.0f, -1.0f,
            0.2f, 0.0f, -1.0f,
            0.0f, 0.1f, -1.0f,
            0.0f, -0.1f, -1.0f,
    };

    public static final float[] BOXES = {
            // X, Y, Z

            // Altitude box
            190.0f, 0.0f, 0.0f,
            320.0f, 0.0f, 0.0f,
            320.0f, 0.0f, 0.0f,
            320.0f, 35f, 0.0f,
            320.0f, 35f, 0.0f,
            190.0f, 35f, 0.0f,
            190.0f, 35f, 0.0f,
            190.0f, 0.0f, 0.0f,
    };

    public float[] pitchLadderVertices;

    public static FloatBuffer getPitchLadderPos(float pitchLadderDeg, int numLines, float scale, float dist) {
        FloatBuffer fbLadder = GLLine.initFloatBuffer(2 + numLines * (PITCH_LINE_COORDS.length / 6));

        float[] scaledModel = new float[16];

        // Add horizon line
        Matrix.setIdentityM(scaledModel, 0);
        Matrix.scaleM(scaledModel, 0, scale, scale, dist);
        transformAndPut(HORIZ_LINE_COORDS, scaledModel, fbLadder, false);

        getPitchLadderCommon(fbLadder, -pitchLadderDeg, numLines, scale, dist, false);

        return fbLadder;
    }

    public static FloatBuffer getPitchLadderNeg(float pitchLadderDeg, int numLines, float scale, float dist) {
        FloatBuffer fbLadder = GLLine.initFloatBuffer(numLines * (PITCH_LINE_COORDS.length / 6));

        getPitchLadderCommon(fbLadder, pitchLadderDeg, numLines, scale, dist, true);

        return fbLadder;
    }

   private static void getPitchLadderCommon(FloatBuffer fbLadder, float pitchLadderDeg, int numLines, float scale, float dist, boolean invertY) {

        float[] scaledModel = new float[16];
        float[] rotate = new float[16];
        float[] rotatedModel = new float[16];
        float deg;

        // We scale here to keep proportions correct upon rotation
        Matrix.setIdentityM(scaledModel, 0);
        Matrix.scaleM(scaledModel, 0, scale, scale, dist);
        deg = pitchLadderDeg;
        for (int line = 0; line < numLines; line++) {
            Matrix.setRotateEulerM(rotate, 0, deg, 0f, 0f);
            Matrix.multiplyMM(rotatedModel, 0, rotate, 0, scaledModel, 0);
            transformAndPut(PITCH_LINE_COORDS, rotatedModel, fbLadder, invertY);

            deg += pitchLadderDeg;
        }
    }

    private static void transformAndPut(float[] vertices, float[] matrix, FloatBuffer fb, boolean invertY) {
        float[] srcVertex = new float[4];
        float[] dstVertex = new float[4];
        float invertYMult;

        if (invertY) {
            invertYMult = -1.0f;
        } else {
            invertYMult = 1.0f;
        }
        for (int vertex = 0; vertex < vertices.length / 3; vertex++) {
            srcVertex[0] = vertices[vertex * 3 + 0];
            srcVertex[1] = vertices[vertex * 3 + 1] * invertYMult;
            srcVertex[2] = vertices[vertex * 3 + 2];
            srcVertex[3] = 1.0f;
            Matrix.multiplyMV(dstVertex, 0, matrix, 0, srcVertex, 0);
            fb.put(dstVertex[0]);
            fb.put(dstVertex[1]);
            fb.put(dstVertex[2]);
        }
    }
}
