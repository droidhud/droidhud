/*
 * Copyright (C) 2012, Collabora Ltd.
 *   Author: Sebastian Dröge <sebastian.droege@collabora.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
 *
 */

#ifndef __GST_APP_DEC_SINK_H__
#define __GST_APP_DEC_SINK_H__

#include <gst/gst.h>


G_BEGIN_DECLS


typedef struct {
  GstFlowReturn (*new_frame)        (gpointer user_data, GstBuffer *frame_buffer);
  gboolean      (*open)             (gpointer user_data);
  gboolean      (*close)            (gpointer user_data);
  gboolean      (*start)            (gpointer user_data);
  gboolean      (*stop)             (gpointer user_data);
  gboolean      (*flush)            (gpointer user_data);
  gboolean      (*set_format)       (gpointer user_data, jobject media_format);
  GstFlowReturn (*drain)            (gpointer user_data);

} GstAppDecoderSinkCallbacks;

void
gst_app_decoder_sink_set_callbacks (GstElement * decodersink_element,
    GstAppDecoderSinkCallbacks * callbacks, gpointer user_data);

G_END_DECLS

#endif /* __GST_AMC_VIDEO_DEC_H__ */
