#include <string.h>
#include <stdint.h>
#include <jni.h>
#include <android/log.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/videooverlay.h>
#define GST_USE_UNSTABLE_API
#include <gst/gl/gstglmemory.h>
//#include <gst/app/gstappsink.h>
#include "gstappdecodersink.h"
#include <pthread.h>

GST_DEBUG_CATEGORY_STATIC (debug_category);
#define GST_CAT_DEFAULT debug_category

/*
 * These macros provide a way to store the native pointer to CustomData, which might be 32 or 64 bits, into
 * a jlong, which is always 64 bits, without warnings.
 */
#if GLIB_SIZEOF_VOID_P == 8
# define GET_CUSTOM_DATA(env, thiz, fieldID) (CustomData *)(*env)->GetLongField (env, thiz, fieldID)
# define SET_CUSTOM_DATA(env, thiz, fieldID, data) (*env)->SetLongField (env, thiz, fieldID, (jlong)data)
#else
# define GET_CUSTOM_DATA(env, thiz, fieldID) (CustomData *)(jint)(*env)->GetLongField (env, thiz, fieldID)
# define SET_CUSTOM_DATA(env, thiz, fieldID, data) (*env)->SetLongField (env, thiz, fieldID, (jlong)(jint)data)
#endif

/* Structure to contain all our information, so we can pass it to callbacks */
typedef struct _CustomData {
  jobject app;                  /* Application instance, used to call its methods. A global reference is kept. */
  GstElement *pipeline;         /* The running pipeline */
  GMainContext *context;        /* GLib context used to run the main loop */
  GMainLoop *main_loop;         /* GLib main loop */
  gboolean initialized;         /* To avoid informing the UI multiple times about the initialization */
  GstElement *decoder_sink;
  GstState state;               /* Current pipeline state */
  GstState target_state;        /* Desired pipeline state, to be set once buffering is complete */
  gboolean is_live;             /* Live streams do not use buffering */
  const char *pipeline_string;
  GstVideoFrame frame;
  gboolean frame_is_valid;
  GstAppDecoderSinkCallbacks cb;    /* callback function pointers */
  jobject codec_obj;
} CustomData;

/* These global variables cache values which are not changing during execution */
static pthread_t gst_app_thread;
static pthread_key_t current_jni_env;
static JavaVM *java_vm;
static jfieldID custom_data_field_id;
static jmethodID set_message_method_id;
static jmethodID set_current_position_method_id;
static jmethodID on_gstreamer_initialized_method_id;
static jmethodID on_encoded_frame_available_method_id;
static jmethodID get_codec_input_buffer_index_method_id;
static jmethodID get_codec_input_buffer_method_id;
static jmethodID on_media_format_available_method_id;

/*
 * Private methods
 */

/* Register this thread with the VM */
static JNIEnv *attach_current_thread (void) {
  JNIEnv *env;
  JavaVMAttachArgs args;

  GST_DEBUG ("Attaching thread %p", g_thread_self ());
  args.version = JNI_VERSION_1_4;
  args.name = NULL;
  args.group = NULL;

  if ((*java_vm)->AttachCurrentThread (java_vm, &env, &args) < 0) {
    GST_ERROR ("Failed to attach current thread");
    return NULL;
  }

  return env;
}

/* Unregister this thread from the VM */
static void detach_current_thread (void *env) {
  GST_DEBUG ("Detaching thread %p", g_thread_self ());
  (*java_vm)->DetachCurrentThread (java_vm);
}

/* Retrieve the JNI environment for this thread */
static JNIEnv *get_jni_env (void) {
  JNIEnv *env;

  if ((env = pthread_getspecific (current_jni_env)) == NULL) {
    env = attach_current_thread ();
    pthread_setspecific (current_jni_env, env);
  }

  return env;
}

/* Change the content of the UI's TextView */
static void set_ui_message (const gchar *message, CustomData *data) {
  JNIEnv *env = get_jni_env ();
  GST_WARNING("Setting message to: %s", message);
  jstring jmessage = (*env)->NewStringUTF(env, message);
  (*env)->CallVoidMethod (env, data->app, set_message_method_id, jmessage);
  if ((*env)->ExceptionCheck (env)) {
    GST_ERROR ("Failed to call Java method");
    (*env)->ExceptionClear (env);
  }
  (*env)->DeleteLocalRef (env, jmessage);
}


/****************** BEGIN Callbacks from Gstreamer videodecoder interface ******************/

/* Called when codec configuration data is received */
static gboolean codec_set_format_cb (
        gpointer pdata, 
        jobject media_format)
{
  JNIEnv *env = get_jni_env ();
  CustomData *data = (CustomData *)pdata;

  (*env)->CallVoidMethod (env, data->app, on_media_format_available_method_id, media_format);
  if ((*env)->ExceptionCheck (env)) {
    GST_ERROR ("Failed to call Java method onMediaFormatAvailable()");
    (*env)->ExceptionClear (env);
    return FALSE;
  }
  return TRUE;
}

/* Called when there is a new codec frame */
static GstFlowReturn codec_new_frame_cb (
        gpointer pdata, 
        GstBuffer *input_buffer)
{
  GstMapInfo minfo;
  jint buffer_idx;
  jlong bb_size;
  jobject bytebuffer_obj;
  jboolean is_keyframe;
  guint8 *bb_array;
  gboolean ret;
  CustomData *data = (CustomData *)pdata;
  JNIEnv *env = get_jni_env ();

  //GST_DEBUG("Got callback");
  /*
  GST_DEBUG("new_frame_cb(): width: %d, height: %d format name: %s", 
          frame->info.width, frame->info.height,
          frame->info.finfo->name);
          */
  GST_DEBUG("new_frame_cb()");

  ret = gst_buffer_map (input_buffer, &minfo, GST_MAP_READ);
  if (ret) {
      GST_DEBUG("new_frame_cb(): Map complete: address: %p size: %d", minfo.data, minfo.size);
  } else {
      GST_ERROR("new_frame_cb(): Failed to map new frame!");
      goto exit_none;
  }

  /* Is this a key frame? */
  if (GST_BUFFER_FLAG_IS_SET(input_buffer, GST_BUFFER_FLAG_DELTA_UNIT)) {
      is_keyframe = JNI_FALSE;
  } else {
      is_keyframe = JNI_TRUE;
  }

  /* Get input buffer index from app's list */
  buffer_idx = (*env)->CallIntMethod (env, data->app, get_codec_input_buffer_index_method_id, is_keyframe);
  if ((*env)->ExceptionCheck (env)) {
    GST_ERROR ("Failed to call Java method getCodecInputBufferIndex()");
    (*env)->ExceptionClear (env);
    buffer_idx = -1;
  }

  if (buffer_idx == -1) {
      goto exit_unmap;
  }

  /* Get corresponding ByteBuffer from codec */
  bytebuffer_obj = (*env)->CallObjectMethod (env, data->app, get_codec_input_buffer_method_id, buffer_idx);
  if ((*env)->ExceptionCheck (env)) {
    GST_ERROR ("Failed to call Java method getCodecInputBuffer()");
    (*env)->ExceptionClear (env);
  }
  /* Get byte array */
  bb_array = (guint8 *)(*env)->GetDirectBufferAddress(env, bytebuffer_obj);
  bb_size = (*env)->GetDirectBufferCapacity(env, bytebuffer_obj);
  (*env)->DeleteLocalRef(env, bytebuffer_obj);

  /* Copy from minfo.data to bb_array */
  if ((jlong)minfo.size > bb_size) {
      GST_ERROR("Cannot copy! minfo.size (%ld) > bb_size (%ld)!", (long)minfo.size, (long)bb_size);
      goto exit_unmap;
  }
  memcpy(bb_array, minfo.data, minfo.size);

  /* Notify application */
  (*env)->CallVoidMethod (env, data->app, on_encoded_frame_available_method_id, buffer_idx, minfo.size);
  if ((*env)->ExceptionCheck (env)) {
    GST_ERROR("Failed to call Java method onEncodedFrameAvailable()");
    (*env)->ExceptionClear (env);
  }

exit_unmap:
  gst_buffer_unmap(input_buffer, &minfo);

exit_none:
  // TODO maybe use other codes?
  return GST_FLOW_OK;
}

// TODO consider connecting more of these signals
static gboolean codec_open_cb ( gpointer pdata )
{
    GST_DEBUG("codec_open_cb(): called");
    return TRUE;
}

static gboolean codec_close_cb ( gpointer pdata )
{
    GST_DEBUG("codec_close_cb(): called");
    return TRUE;
}

static gboolean codec_start_cb ( gpointer pdata )
{
    GST_DEBUG("codec_start_cb(): called");
    return TRUE;
}

static gboolean codec_stop_cb ( gpointer pdata )
{
    GST_DEBUG("codec_stop_cb(): called");
    return TRUE;
}

static gboolean codec_flush_cb ( gpointer pdata )
{
    GST_DEBUG("codec_flush_cb(): called");
    return TRUE;
}

static gboolean codec_drain_cb ( gpointer pdata )
{
    GST_DEBUG("codec_drain_cb(): called");
    return TRUE;
}

/******************** END Callbacks from Gstreamer videodecoder interface ******************/

/******************** BEGIN Callbacks from Gstreamer pipeline ******************************/

/* Retrieve errors from the bus and show them on the UI */
static void error_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  GError *err;
  gchar *debug_info;
  gchar *message_string;

  gst_message_parse_error (msg, &err, &debug_info);
  message_string = g_strdup_printf ("Error received from element %s: %s", GST_OBJECT_NAME (msg->src), err->message);
  g_clear_error (&err);
  g_free (debug_info);
  set_ui_message (message_string, data);
  g_free (message_string);
  data->target_state = GST_STATE_NULL;
  gst_element_set_state (data->pipeline, GST_STATE_NULL);
}

/* Notify UI about pipeline state changes */
static void state_changed_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  GstState old_state, new_state, pending_state;
  gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
  /* Only pay attention to messages coming from the pipeline, not its children */
  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (data->pipeline)) {
    data->state = new_state;
    gchar *message = g_strdup_printf("State changed to %s", gst_element_state_get_name(new_state));
    set_ui_message(message, data);
    g_free (message);

    if (new_state == GST_STATE_NULL || new_state == GST_STATE_READY)
      data->is_live = FALSE;

    /* The Ready to Paused state change is particularly interesting: */
    if (old_state == GST_STATE_PAUSED && new_state == GST_STATE_PLAYING) {
      /* By now the sink already knows the media size */
      //check_media_size(data);
    }
  }
}

/********************** END Callbacks from Gstreamer pipeline ******************************/

/* Check if all conditions are met to report GStreamer as initialized.
 * These conditions will change depending on the application */
static void check_initialization_complete (CustomData *data) {
  JNIEnv *env = get_jni_env ();
  if (!data->initialized && data->main_loop) {
    GST_INFO("Initialization complete, notifying application. main_loop:%p", data->main_loop);

    data->target_state = GST_STATE_READY;
    gst_element_set_state(data->pipeline, GST_STATE_READY);

    (*env)->CallVoidMethod (env, data->app, on_gstreamer_initialized_method_id);
    if ((*env)->ExceptionCheck (env)) {
      GST_ERROR ("Failed to call Java method");
      (*env)->ExceptionClear (env);
    }
    data->initialized = TRUE;
  }
}

/* Main method for the native code. This is executed on its own thread. */
static void *app_function (void *userdata) {
  JNIEnv *env = get_jni_env ();
  JavaVMAttachArgs args;
  GstBus *bus;
  CustomData *data = (CustomData *)userdata;
  GSource *timeout_source;
  GSource *bus_source;
  GError *error = NULL;
  jlong eglDisplayHandle;
  jlong eglContextHandle;
  GstGLDisplay *gl_display;
  GstGLContext *gl_context;
  GstStructure *gst_s;

  GValue elem = G_VALUE_INIT;
  GstIterator *iter;
  GstIteratorResult iter_res;
  gchar *name;
  GstAppDecoderSinkCallbacks callbacks;

  GST_DEBUG ("Creating pipeline in CustomData at %p", data);

  /* Create our own GLib Main Context and make it the default one */
  data->context = g_main_context_new ();
  g_main_context_push_thread_default(data->context);

  /* Build pipeline */
  data->pipeline = gst_parse_launch(data->pipeline_string, &error);
  if (error) {
    gchar *message = g_strdup_printf("Unable to build pipeline: %s", error->message);
    g_clear_error (&error);
    set_ui_message(message, data);
    g_free (message);
    return NULL;
  }

  data->decoder_sink = gst_bin_get_by_name(GST_BIN(data->pipeline), "decoder");
  if (!data->decoder_sink) {
    GST_ERROR ("Could not retrieve app sink");
    return NULL;
  }

#if 0
  /* Listen for new samples */
  g_signal_connect (data->decoder_sink, "new-sample", (GCallback)new_sample_cb, data);
#endif

  iter = gst_bin_iterate_elements(GST_BIN(data->pipeline));
  GST_DEBUG("Got element iterator");
  iter_res = gst_iterator_next(iter, &elem);

  while (GST_ITERATOR_OK == iter_res) {
      GST_DEBUG("Got an element");
      GST_DEBUG("Element name: %s", gst_element_get_name(g_value_get_object(&elem)));
      g_value_reset(&elem);
      iter_res = gst_iterator_next(iter, &elem);
  }

  g_value_unset(&elem);
  gst_iterator_free(iter);

  /* Set the pipeline to READY, so it can already accept a window handle, if we have one */
  /*
  data->target_state = GST_STATE_READY;
  gst_element_set_state(data->pipeline, GST_STATE_READY);
  */

  /* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
  bus = gst_element_get_bus (data->pipeline);
  bus_source = gst_bus_create_watch (bus);
  g_source_set_callback (bus_source, (GSourceFunc) gst_bus_async_signal_func, NULL, NULL);
  g_source_attach (bus_source, data->context);
  g_source_unref (bus_source);
  g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)error_cb, data);
  //g_signal_connect (G_OBJECT (bus), "message::eos", (GCallback)eos_cb, data);
  g_signal_connect (G_OBJECT (bus), "message::state-changed", (GCallback)state_changed_cb, data);
  //g_signal_connect (G_OBJECT (bus), "message::buffering", (GCallback)buffering_cb, data);
  //g_signal_connect (G_OBJECT (bus), "message::clock-lost", (GCallback)clock_lost_cb, data);
  gst_object_unref (bus);

  /* Register a function that GLib will call 4 times per second */
  /*
  g_source_set_callback (timeout_source, (GSourceFunc)refresh_ui, data, NULL);
  g_source_attach (timeout_source, data->context);
  g_source_unref (timeout_source);
  */

  // Initialize callbacks for appdecodersink
  callbacks.new_frame = codec_new_frame_cb;
  callbacks.open = codec_open_cb;
  callbacks.close = codec_close_cb;
  callbacks.start = codec_start_cb;
  callbacks.stop = codec_stop_cb;
  callbacks.flush = codec_flush_cb;
  callbacks.set_format = codec_set_format_cb;
  callbacks.drain = codec_drain_cb;
  gst_app_decoder_sink_set_callbacks (data->decoder_sink, &callbacks, data);


  /* Create a GLib Main Loop and set it to run */
  GST_INFO("Entering main loop... (CustomData:%p)", data);
  data->main_loop = g_main_loop_new (data->context, FALSE);
  check_initialization_complete (data);
  g_main_loop_run (data->main_loop);
  GST_INFO("Exited main loop");
  g_main_loop_unref (data->main_loop);
  data->main_loop = NULL;

  /* Free resources */
  g_main_context_pop_thread_default(data->context);
  g_main_context_unref (data->context);
  data->target_state = GST_STATE_NULL;
  gst_element_set_state (data->pipeline, GST_STATE_NULL);
  gst_object_unref (data->decoder_sink);
  gst_object_unref (data->pipeline);

  return NULL;
}

/*
 * Java Bindings
 */

/* Instruct the native code to create its internal data structure, pipeline and thread */
static void gst_native_init (JNIEnv* env, jobject thiz, jstring pipeline) {
  CustomData *data = g_new0 (CustomData, 1);
  const char *pipeline_string;
  jsize length;
 
  /* save pipeline */
  length = (*env)->GetStringUTFLength(env, pipeline);
  pipeline_string = (*env)->GetStringUTFChars (env, pipeline, NULL);
  data->pipeline_string = (const char *)calloc(length + 1, 1);
  memcpy((void *)data->pipeline_string, (void *)pipeline_string, length);

  data->frame_is_valid = FALSE;

  SET_CUSTOM_DATA (env, thiz, custom_data_field_id, data);
  GST_DEBUG_CATEGORY_INIT (debug_category, "libglvideogs2", 0, "GLVideoGS2");
  gst_debug_set_threshold_for_name("libglvideogs2", GST_LEVEL_WARNING);
  //gst_debug_set_threshold_for_name("GST_PIPELINE", GST_LEVEL_DEBUG);
  gst_debug_set_threshold_for_name("amc*", GST_LEVEL_WARNING);
  //gst_debug_set_threshold_for_name("gl*", GST_LEVEL_DEBUG);
  //gst_debug_set_threshold_for_name("videoconvert", GST_LEVEL_DEBUG);
  //gst_debug_set_threshold_for_name("appsink", GST_LEVEL_WARNING);
  GST_DEBUG ("Created CustomData at %p", data);
  data->app = (*env)->NewGlobalRef (env, thiz);
  GST_DEBUG ("Created GlobalRef for app object at %p", data->app);
  pthread_create (&gst_app_thread, NULL, &app_function, data);
}

/* Quit the main loop, remove the native thread and free resources */
static void gst_native_finalize (JNIEnv* env, jobject thiz) {
  CustomData *data = GET_CUSTOM_DATA (env, thiz, custom_data_field_id);
  if (!data) return;

  free((void *)data->pipeline_string);

  GST_DEBUG ("Quitting main loop...");
  g_main_loop_quit (data->main_loop);
  GST_DEBUG ("Waiting for thread to finish...");
  pthread_join (gst_app_thread, NULL);
  GST_DEBUG ("Deleting GlobalRef for app object at %p", data->app);
  (*env)->DeleteGlobalRef (env, data->app);
  GST_DEBUG ("Freeing CustomData at %p", data);
  g_free (data);
  SET_CUSTOM_DATA (env, thiz, custom_data_field_id, NULL);
  GST_DEBUG ("Done finalizing");
}

/* Set pipeline to PLAYING state */
static void gst_native_play (JNIEnv* env, jobject thiz) {
  CustomData *data = GET_CUSTOM_DATA (env, thiz, custom_data_field_id);
  if (!data) return;
  GST_INFO("Setting state to PLAYING");
  data->target_state = GST_STATE_PLAYING;
  data->is_live |= (gst_element_set_state (data->pipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_NO_PREROLL);
}

/* Set pipeline to PAUSED state */
static void gst_native_pause (JNIEnv* env, jobject thiz) {
  CustomData *data = GET_CUSTOM_DATA (env, thiz, custom_data_field_id);
  if (!data) return;
  GST_INFO("Setting state to PAUSED");
  data->target_state = GST_STATE_PAUSED;
  data->is_live |= (gst_element_set_state (data->pipeline, GST_STATE_PAUSED) == GST_STATE_CHANGE_NO_PREROLL);
}

/* Static class initializer: retrieve method and field IDs */
static jboolean gst_native_class_init (JNIEnv* env, jclass klass) {
  custom_data_field_id = (*env)->GetFieldID (env, klass, "native_custom_data", "J");
  set_message_method_id = (*env)->GetMethodID (env, klass, "setMessage", "(Ljava/lang/String;)V");
  on_gstreamer_initialized_method_id = (*env)->GetMethodID (env, klass, "onGStreamerInitialized", "()V");
  on_encoded_frame_available_method_id = (*env)->GetMethodID (env, klass, "onEncodedFrameAvailable", "(II)V");
  get_codec_input_buffer_index_method_id = (*env)->GetMethodID (env, klass, "getCodecInputBufferIndex", "(Z)I");
  get_codec_input_buffer_method_id = (*env)->GetMethodID (env, klass, "getCodecInputBuffer", "(I)Ljava/nio/ByteBuffer;");
  on_media_format_available_method_id = (*env)->GetMethodID (env, klass, "onMediaFormatAvailable", "(Landroid/media/MediaFormat;)V");

  if (!custom_data_field_id || 
        !on_encoded_frame_available_method_id ||
        !set_message_method_id || 
        !on_gstreamer_initialized_method_id ||
        !get_codec_input_buffer_method_id ||
        !on_media_format_available_method_id ||
        !get_codec_input_buffer_index_method_id)
  {
    /* We emit this message through the Android log instead of the GStreamer log because the later
     * has not been initialized yet.
     */
    __android_log_print (ANDROID_LOG_ERROR, "libglvideogs2", "The calling class does not implement all necessary interface methods");
    return JNI_FALSE;
  }
  return JNI_TRUE;
}

/* List of implemented native methods */
static JNINativeMethod native_methods[] = {
  { "nativeInit", "(Ljava/lang/String;)V", (void *) gst_native_init},
  { "nativeFinalize", "()V", (void *) gst_native_finalize},
  { "nativePlay", "()V", (void *) gst_native_play},
  { "nativePause", "()V", (void *) gst_native_pause},
  { "nativeClassInit", "()Z", (void *) gst_native_class_init}
};

/* Library initializer */
jint JNI_OnLoad(JavaVM *vm, void *reserved) {
  JNIEnv *env = NULL;

  java_vm = vm;

  if ((*vm)->GetEnv(vm, (void**) &env, JNI_VERSION_1_4) != JNI_OK) {
    __android_log_print (ANDROID_LOG_ERROR, "libglvideogs2", "Could not retrieve JNIEnv");
    return 0;
  }
  jclass klass = (*env)->FindClass (env, "org/fuster/droidhud/GLVideoGS2");
  (*env)->RegisterNatives (env, klass, native_methods, G_N_ELEMENTS(native_methods));

  pthread_key_create (&current_jni_env, detach_current_thread);

  return JNI_VERSION_1_4;
}
/* vim: set ts=2:sw=2: */
