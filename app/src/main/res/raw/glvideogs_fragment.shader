precision mediump float;
varying vec2 vTextureCoord;
uniform sampler2D sTexture;

void main() {
    gl_FragColor.rgb = texture2D(sTexture, vTextureCoord).rgb;
    gl_FragColor.a = 1.0;
}