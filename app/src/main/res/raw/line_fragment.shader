precision mediump float;
uniform vec4 u_Color;
uniform vec4 u_ColorAlt;
uniform float u_ColorLen;
varying vec2 v_xy;
bool altColor_x;
bool altColor_y;

void main() {
    altColor_x = (mod(v_xy.x, u_ColorLen*2.0) >= u_ColorLen) ? true : false;
    altColor_y = (mod(v_xy.y, u_ColorLen*2.0) >= u_ColorLen) ? true : false;
    gl_FragColor = (altColor_x ^^ altColor_y) ? u_ColorAlt : u_Color;
}
