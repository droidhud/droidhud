uniform mat4 u_MVP;
attribute vec4 a_Position;
varying vec2 v_xy;

void main()
{
    v_xy = a_Position.xy;
    gl_Position = u_MVP * a_Position;
}
